# README #

This Discord bot will send a message to a text channel when specific user uploads a new video on Youtube or tweets. 

## Variables to change in the bot.py

```python
msg_channel_id = # discord text channel id that the bot will send messages

notification_role_id = # discord role id that it will get notified when sending a message about new video or tweet
```

## Variables to change in the config.ini

Create config.ini file in the venv/ directory
```
dc_username = # username that gets mentioned in the message when tweet is tweeted (tweet sender)

[BOT]
bot_token = # your discord bots token
bot_prefix = # prefix for bot

[TWITTER]
bearer_token = # your twitter developer bearer token
handle = # twitter handle

[YOUTUBE]
api_key = # your google developer api key
channel_id = # youtube channel id that the bot will fetch subscriber count and new video data

[SETTINGS]
color = 1942002 # embed messages side bar color

[DELAY]
sleep_time_video = 15 # how long will it take to check for new video (in minutes) (default = 15 min)
sleep_time_subs = 24 # how long will it take to check for sub count (in hours) (default = 24 hours)
sleep_time_tweet = 10 # how long will it take to check for new tweets (in minutes) (default = 10 min)
```

## Example image of the tweet that will be sent

![example](images/tweet_example.png)