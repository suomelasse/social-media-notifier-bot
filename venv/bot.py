import configparser
import requests
import json
from urllib.request import urlopen
import regex as re
import asyncio
import discord
from discord.ext import commands
from discord.ext.commands import when_mentioned_or
from discord.utils import get
from discord.ext.tasks import loop
from datetime import datetime

# read config values
cfg = configparser.ConfigParser()
cfg.read('config.ini')

bot_token = cfg.get('BOT', 'bot_token')  # dc bot token
channel_id = cfg.get('YOUTUBE', 'channel_id')  # youtube channel id
api_key = cfg.get('YOUTUBE', 'api_key')  # google developer api key

bearer_token = cfg.get('TWITTER', 'bearer_token')
username = cfg.get('TWITTER', 'handle')

msg_channel_id = 870302292958674944  # dc text channel id
test_channel_id = 870340376957972521  # dc test text channel
notification_role_id = 870340528414289920  # role id that gets mentioned

dc_username = 'Pepe Vieri'

sleep_time = cfg.getint('DELAY', 'sleep_time_video')  # how long will it take to check for new video (in minutes) (default = 30 min)
sleep_time_subs = cfg.getint('DELAY', 'sleep_time_subs')  # how long will it take to check for sub count (in hours) (default = 24 h)
sleep_time_tweet = cfg.getint('DELAY', 'sleep_time_tweet')  # how long will it take to check for new tweets (in minutes) (default = 5 min)

sub_url = f'https://www.googleapis.com/youtube/v3/channels?part=statistics&id={channel_id}&key={api_key}'

video_url = f'https://www.googleapis.com/youtube/v3/search?key={api_key}&channelId={channel_id}&part=snippet,id&order=date&maxResults=1'
video_link = 'http://youtu.be/'

timelineUrl = f'https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name={username}&count=1&include_rts=false&exclude_replies=true&tweet_mode=extended'

bot = commands.Bot(command_prefix=when_mentioned_or(cfg.get('BOT', 'bot_prefix')), case_insensitive=True)

bot.prevVideoId = ''
bot.prevTweetId = ''


@bot.event
async def on_ready():
    print(f'Logged in as: \n{bot.user.name}/{bot.user.id}')
    print('-' * 30)

    try:
        # read video id from file
        f = open('videoId.txt', 'r')
        bot.prevVideoId = f.readline()
        print(f'{datetime.now().replace(microsecond=0)} - Video tag from file is: {bot.prevVideoId}')
        f.close()

        # read tweet id from file
        f = open('tweetId.txt', 'r')
        bot.prevTweetId = f.readline()
        print(f'{datetime.now().replace(microsecond=0)} - Tweet id from file is: {bot.prevTweetId}')
        f.close()

        # start all tasks
        fetch_video_data.start()
        fetch_sub_count.start()
        fetch_tweets.start()
        print('Started all tasks')

    except Exception as e:
        print(e)


@loop(minutes=sleep_time)
async def fetch_video_data():

    isValidId = False

    # get newest videos tag
    try:
        newVideoTag = get_data(video_url)['items'][0]['id']['videoId']
        if len(newVideoTag) > 0:
            isValidId = True
            print(f'Valid video id: {newVideoTag}')
    except Exception as e:
        print(f'{datetime.now().replace(microsecond=0)} VideoID not found. Google API quota exceeded {e}')

    if newVideoTag != bot.prevVideoId and isValidId:
        bot.prevVideoId = newVideoTag

        # open file and remove everything in it and write new video id
        f = open('videoId.txt', 'a+')
        f.truncate(0)
        f.write(bot.prevVideoId)
        print(f'{datetime.now().replace(microsecond=0)} - Wrote new video id to file: {bot.prevVideoId}')
        f.close()

        # print(f'<@&{role_id}> Check out my new video on Youtube\n{video_link+newVideoTag}')

        # send message
        try:
            channel = bot.get_channel(msg_channel_id)
            await channel.send(f'<@&{notification_role_id}> **{dc_username}** uploaded a new video\n{video_link + newVideoTag}')
            print(f'{datetime.now().replace(microsecond=0)} - Sent notification about new video on dc channel.')
        except Exception as e:
            print(e)
    else:
        print(f'{datetime.now().replace(microsecond=0)} - No new video was found')


@loop(hours=sleep_time_subs)
async def fetch_sub_count():
    # get sub count from yt channel
    try:
        subscriberCount = int(get_data(sub_url)['items'][0]['statistics']['subscriberCount'])

        await bot.change_presence(
            activity=discord.Activity(type=discord.ActivityType.watching, name=f'{human_format(subscriberCount)} subs'))
        print(f'{datetime.now().replace(microsecond=0)} - Changed bots status. Changed sub count to: {human_format(subscriberCount)}')
    except Exception as e:
        print(f'{datetime.now().replace(microsecond=0)} Sub count not found. Not updating sub count. Google API quota exceeded {e}')


@loop(minutes=sleep_time_tweet)
async def fetch_tweets():
    # get twitter users timeline from API
    try:
        timeline = json.loads(get_tweet_data(timelineUrl))
    except Exception as e:
        print(e)

    isValidTweetId = False
    tweetId = ''

    try:
        print('Trying to fetch new tweet')
        # get tweet text and id
        newestTweet = timeline[0]['full_text']
        tweetId = timeline[0]['id_str']
        # tweetCreatedAt = timeline[0]['created_at']
        profile_image_url_https = timeline[0]['user']['profile_image_url_https']
        screen_name = timeline[0]['user']['screen_name']
        name = timeline[0]['user']['name']
        media_url_https = ''

        if len(tweetId) > 0:
            isValidTweetId = True
            print(f'Valid tweet id: {tweetId}')

    except Exception as ex:
        print(f'fetch_tweets error: {ex}')

    #print(f'tweetid= {tweetId} bot.previd= {bot.prevTweetId} validtweet= {isValidTweetId}') # debugging
    if tweetId != bot.prevTweetId and isValidTweetId:
        bot.prevTweetId = tweetId

        # open file and remove everything in it and write new video id
        f = open('tweetId.txt', 'a+')
        f.truncate(0)
        f.write(bot.prevTweetId)
        print(f'{datetime.now().replace(microsecond=0)} - Wrote new tweet id to file: {bot.prevTweetId}')
        f.close()

        # find all urls in the tweet
        try:
            urls = re.findall(r'(https?://[^\s]+)', newestTweet)
            print(f'{datetime.now().replace(microsecond=0)} - Found {len(urls)} urls')
        except Exception as e:
            print(e)

        if len(urls) > 1:

            linkToTweet = urls[-1]

            # remove link from the tweet
            tweetWithoutLink = newestTweet.replace(linkToTweet, '')

            # add '<>' to the links so it disables discords link preview
            for i in range(len(urls)):
                oldUrl = urls[i]
                urls[i] = '<' + urls[i] + '>'
                tweetWithoutLink = tweetWithoutLink.replace(oldUrl, urls[i])

            try:
                media_url_https = timeline[0]['extended_entities']['media'][0]['media_url_https']
            except Exception as e:
                print(e)

            print(f'{datetime.now().replace(microsecond=0)} - {urls}')
            print(f'{datetime.now().replace(microsecond=0)} - {timeline}')

        elif len(urls) == 1:
            print(f'{datetime.now().replace(microsecond=0)} - {urls}')
            print(f'{datetime.now().replace(microsecond=0)} - {timeline}')

        # send message
        try:
            embed = discord.Embed()
            embed.color = cfg.getint('SETTINGS', 'color')  # mapinfo color of side bar

            if len(urls) > 1:
                # tweet with links
                embed.description = f'<@&{notification_role_id}> **{name}** tweeted: \n\n{tweetWithoutLink}'  # body of embed

                if len(media_url_https) > 5:
                    embed.set_image(url=media_url_https)

            elif len(urls) == 1:
                # tweet with links
                embed.description = f'<@&{notification_role_id}> **{name}** tweeted: \n\n{newestTweet}'  # body of embed

                if len(media_url_https) > 5:
                    embed.set_image(url=media_url_https)

            else:
                # only text tweet
                embed.description = f'<@&{notification_role_id}> **{name}** tweeted: \n\n{newestTweet}'  # body of embed

            embed.set_author(name=f'{name} (@{screen_name})', url=f'https://twitter.com/i/web/status/{tweetId}',
                             icon_url=profile_image_url_https)  # set link to tweet
            embed.set_footer(text=f'Twitter',
                             icon_url='https://abs.twimg.com/favicons/twitter.ico')  # set footer text and icon

            try:
                channel = bot.get_channel(msg_channel_id)
                await channel.send(embed=embed)
                print(f'{datetime.now().replace(microsecond=0)} - Sent message about new tweet')
            except Exception as e:
                print(e)
        except Exception as e:
            print(e)
    else:
        print(f'{datetime.now().replace(microsecond=0)} - No new tweet was found')


# get data from youtube api
def get_data(url):
    try:
        response = urlopen(url)
        # from url in data
        data = json.loads(response.read())
    except Exception as e:
        print(e)
    return data


def get_tweet_data(url):
    try:
        headers = {'Authorization': f'Bearer {bearer_token}'}

        response = requests.request('GET', url, headers=headers)

        tweetsData = response.json()
        tweets = json.dumps(tweetsData, indent=4, sort_keys=True)
        print(response)
    except Exception as e:
        print(e)

    return tweets


# convert number to human readable form with K M suffixes
def human_format(num):
    magnitude = 0

    while abs(num) >= 1000:
        magnitude += 1
        num /= 1000

    if magnitude == 0:
        return '%.0f%s' % (num, ['', 'K', 'M'][magnitude])
    else:
        return '%.1f%s' % (num, ['', 'K', 'M'][magnitude])


# start bot
try:
    bot.run(bot_token)
except Exception as e:
    print(e)
